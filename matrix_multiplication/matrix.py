from mrjob.job import MRJob
from mrjob.step import MRStep

class MatrixMultiplication(MRJob):

    def mapper_first(self, key, line):
        mat, i, j, val = line.split(',')
        i, j, val = int(i), int(j), float(val)
        if mat == 'M':
            yield j, ('M', i, val)
        elif mat == 'N':
            yield i, ('N', j, val)
        else:
            raise ValueError('Invalid matrix name')

    def reducer_first(self, key, values):
        mat_M, mat_N = [], []
        for mat, k, val in values:
            if mat == 'N':
                mat_N.append({'key': k, 'value': val})
            if mat == 'M':
                mat_M.append({'key': k, 'value': val})
        for j in mat_N:
            for i in mat_M:
                yield (i['key'], j['key']), i['value'] * j['value']

    def reducer_second(self, key, values):
        yield key, sum(values)

    def steps(self):
        return [
            MRStep(
                mapper=self.mapper_first,
                reducer=self.reducer_first
            ),
            MRStep(
                reducer=self.reducer_second
            )
        ]
    
    
if __name__ == '__main__':
    MatrixMultiplication.run()
