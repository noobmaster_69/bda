from mrjob.job import MRJob

class Sales(MRJob):
    
    def mapper(self, key, value):
        record = value.strip().split(',')
        yield record[7], 1
                    
    def reducer(self, key, values):
        yield key, f'{sum(values)} item(s) are sold'
        
if __name__ == '__main__':
    Sales.run()
