from mrjob.job import MRJob
from datetime import datetime

class WeatherStats(MRJob):
    
    def mapper(self, key, value):
        records = value.split(',') 
        for i in range(1, len(records) - 1, 2):
            yield records[0], (records[i], float(records[i + 1]))
    
    def reducer(self, key, values):
        _min = float('inf')
        _max = float('-inf')
        hr1, hr2 = 0, 0
        for h, temp in values:
            if temp < _min:
                hr1, _min = h, temp
            if temp >= _max:
                hr2, _max = h, temp
        yield f'{key}', f'Time: {hr1} MinTemp: {_min} Time: {hr2} MaxTemp {_max}' 
        
if __name__ == '__main__':
    WeatherStats.run()

