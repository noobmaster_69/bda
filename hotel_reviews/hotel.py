from mrjob.job import MRJob

# input file: Hotel_Reviews_Slip9.csv
# for other dataset make required changes before use
class HotelStats(MRJob):
    
    def mapper(self, key, value):
        try:
            review, rating = value.split('\",')
            yield int(rating), 1
        except ValueError:
            pass
        
    def reducer(self, key, values):
        yield key, f'{sum(values)} reviews have {key} star(s)'
        
if __name__ == '__main__':
    HotelStats.run()