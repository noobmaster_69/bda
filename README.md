### Map-reduce Index

| Slip # | Directory               |
|--------|-------------------------|
| 1, 15  | wordcount               |
| 2, 14  | weather_temperature     |
| 3      | weather_report          |
| 4      | electricity_consumption |
| 5      | website_visitors        |
| 6, 13  | sales                   |
| 7      | wordcount_dear_bear     |
| 8, 11  | matrix_multiplication   |
| 9      | hotel_reviews           |
| 10     | employee_min_max        |
| 12     | employee_freq           |

### Steps to execute Python MR programs

#### For running locally

1. Execute
   ```bash
   python3 <program_name>.py <dataset_name>
   ```

#### For running on HDFS

1. Execute
    > Make sure you have your dataset in HDFS.
    
    ```bash
    python3 <program_name>.py -r hadoop <hdfs_loc>
    ```

    `<hdfs_loc>` is an URI, for e.g. `hdfs:///test/data.txt`

### Installing `micro` editor

1. Download executable: 

    ```bash
    curl https://getmic.ro | bash
    ```

    Now you'll have `micro` binary in your current working directory. 

2. Move to one of the directory mentioned in `$PATH` :

    _Accustomed setup to our labs_

    ```bash
    mkdir -p ~/perl/bin
    mv micro ~/perl/bin/
    ```

    Now, `micro` should be accessible from any location from console.

3. (Optional) Install plugins to add extra functionality to `micro`

    ```bash
    micro -plugin install quoter
    ```

    For listing all plugins available from repositories:

    ```bash
    micro -plugin available
    ```

    For listing locally installed plugins:

    ```bash
    micro -plugin list

### References:

- [Hadoop FS Shell Commands - Apache Hadoop Docs](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/CommandsManual.html)